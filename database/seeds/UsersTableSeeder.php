<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $user_list = array(
            array(
                'name'=>'Admin User',
                'email'=>'admin@admin.com',
                'password'=> Hash::make('admin1234'),
                'role'=>'admin'

            ),

            array(
                'name'=>'School User',
                'email'=>'school@school.com',
                'password'=> Hash::make('school1234'),
                'role'=>'school'

            ),

                array(
                'name'=>'Parent User',
                'email'=>'parent@parent.com',
                'password'=> Hash::make('parent1234'),
                'role'=>'parent'

            )


        );

        DB::table('users')->insert($user_list);




    }
}
