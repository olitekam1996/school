<?php

function uploadImage($file,$dir,$thumb =null){
 $path = public_path().'/uploads/'.$dir;
 if (!File::exists($path)){
     File::makeDirectory($path,0777,true,true);
 }
 $image_name = ucfirst($dir)."-".date('Ymdhis').rand(0,9999).".".$file->getClientOriginalExtension();
 $success = $file->move($path,$image_name);
  if ($success){
      if ($thumb){
        list($width,$height)= explode('x',$thumb); //1200x700
          Image::make($path.'/'.$image_name)->resize($width,$height,function ($constraint){
              return $constraint->aspectRatio();
          })->save($path.'/Thumb-'.$image_name);
      }
      return $image_name;
  } else{
     return false;
  }
}



function deleteImage($image_name,$dir){
    $path = public_path().'/uploads/'.$dir;

    if ($image_name !=null && file_exists($path.'/'.$image_name)){
        unlink($path.'/'.$image_name);
    }

    if ($image_name !=null && file_exists($path.'/Thumb-'.$image_name)){
        unlink($path.'/Thumb-'.$image_name);
    }
}



function getImage($dir, $filename){
    if(file_exists(public_path('uploads/'.$dir.'/'.'Thumb-'.$filename))){
        return asset('uploads/'.$dir.'/'.'Thumb-'.$filename);
    }else{
        return asset('uploads/'.$dir.'/'.$filename);
    }
}









function deleteFile($file_name,$dir){
    $path = public_path().'/uploads/files/'.$dir;

    if ($file_name !=null && file_exists($path.'/'.$file_name)){
        unlink($path.'/'.$file_name);
    }

    if ($file_name !=null && file_exists($path.$file_name)){
        unlink($path.$file_name);
    }
}





function uploadFile($file,$dir){
    $path = public_path().'/uploads/files/'.$dir;
    if (!File::exists($path)){
        File::makeDirectory($path,0777,true,true);
    }
    $file_name = ucfirst($dir)."-".date('Ymdhis').rand(0,9999).".".$file->getClientOriginalExtension();
    $success = $file->move($path,$file_name);
    if ($success){

        return $file_name;
    } else{
        return false;
    }
}







