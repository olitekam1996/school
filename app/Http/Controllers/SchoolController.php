<?php

namespace App\Http\Controllers;

use App\Models\School;
use Illuminate\Http\Request;

class SchoolController extends Controller
{

    protected $school = null;

    public function __construct(School $school)

    {
        $this->school = $school;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->school = $this->school->with('created_by')->orderBy('id', 'DESC')->paginate();

        return view('admin.school')->with('data', $this->school);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.school-form');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = $this->school->getRules();
        $request->validate($rules);
        $data = $request->except('image');
        if ($request->image) {
            $image_name = uploadImage($request->image, 'school', '200x300');
            if ($image_name) {
                $data['image'] = $image_name;
            }
        }
        $data['added_by'] = $request->user()->id;
        $this->school->fill($data);
        $success = $this->school->save();
        if ($success) {
            $request->session()->flash('success', 'School  added successfully.');
        } else {
            $request->session()->flash('error', 'Sorry! There wss a problem while adding school.');
        }
        return redirect()->route('school.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

         $this->school = $this->school->where('id',$id)->get();
         //dd($this->school);
        return view('admin.school-view-form')->with('school', $this->school);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->school = $this->school->find($id);
        if (!$this->school) {
            request()->session()->flash('error', 'School does  not exists.');
            return redirect()->route('school.index');
        }
        return view('admin.school-form')->with('data', $this->school);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->school = $this->school->find($id);
        if (!$this->school) {
            request()->session()->flash('error', 'School does  not exists.');
            return redirect()->route('school.index');
        }

        $rules = $this->school->getRules("update");
        $request->validate($rules);
        $data = $request->except('image');
        if ($request->image) {
            $image_name = uploadImage($request->image, 'school', '1200x720');
            if ($image_name) {
                $data['image'] = $image_name;
                deleteImage($this->school->image, 'school');
            }
        }

        $this->school->fill($data);
        $success = $this->school->save();
        if ($success) {
            $request->session()->flash('success', 'School updated successfully.');
        } else {
            $request->session()->flash('error', 'Sorry! There was a problem while updating  school.');
        }
        return redirect()->route('school.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $this->school = $this->school->find($id);
        if (!$this->school) {
            request()->session()->flash('error', 'School does  not exists.');
            return redirect()->route('school.index');
        }
        $image = $this->school->image;
        $del = $this->school->delete();
        if ($del) {
            deleteImage($image, 'school');
            request()->session()->flash('success', 'School deleted successfully.');

        } else {
            request()->session()->flash('error', 'Sorry! there was a problem while deleting school.');
        }
        return redirect()->route('school.index');


    }
}


