<?php

namespace App\Http\Controllers;

use App\Models\Grade;
use Illuminate\Http\Request;


class GradeController extends Controller
{
    protected $grade = null;
    public  function __construct(Grade $grade)
    {
     $this->grade = $grade;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->grade = $this->grade->with('created_by')->orderBy('id', 'DESC')->paginate();
        return view('admin.grade')->with('data', $this->grade);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.grade-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = $this->grade->getrules();
        $data = $request->validate($rules);

        $data['added_by']=$request->user()->id;

        $this->grade->fill($data);
        if ($this->grade->save()){
            $request->session()->flash('success','Class added successfully.');
        } else{
            $request->session()->flash('error','sorry! There was a problem while adding Class.');


        }
        return redirect()->route('grade.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->grade = $this->grade->find($id);
        if (!$this->grade) {
            request()->session()->flash('error', 'Class does  not exists.');
            return redirect()->route('grade.index');
        }
        return view('admin.grade-form')->with('data', $this->grade);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->grade = $this->grade->find($id);
        if (!$this->grade) {
            request()->session()->flash('error', 'Class does  not exists.');
            return redirect()->route('grade.index');
        }

        $rules = $this->grade->getrules();
        $data = $request->validate($rules);

        $data['added_by']=$request->user()->id;

        $this->grade->fill($data);
        if ($this->grade->save()){
            $request->session()->flash('success','Class  Updated  successfully.');
        } else{
            $request->session()->flash('error','sorry! There was a problem while updating  Class.');


        }
        return redirect()->route('grade.index');




    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
//        dd($id);
        $this->grade = $this->grade->find($id);
        if (!$this->grade) {
            request()->session()->flash('error', 'Class does  not exists.');
            return redirect()->route('grade.index');
        }
        $del = $this->grade->delete();
        if ($del) {
            request()->session()->flash('success', 'Class deleted successfully.');
        } else {
            request()->session()->flash('error', 'Sorry! there was a problem while deleting Class.');
        }
        return redirect()->route('grade.index');

    }




}
