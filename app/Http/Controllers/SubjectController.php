<?php

namespace App\Http\Controllers;
use App\Models\Subject;
use Illuminate\Http\Request;

class SubjectController extends Controller
{


    protected $subject = null;

    public function __construct(Subject $subject)

    {
        $this->subject = $subject;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->subject = $this->subject->with('created_by')->orderBy('id', 'DESC')->paginate();

        return view('admin.subject')->with('data', $this->subject);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.subject-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = $this->subject->getRules();



        $request->validate($rules);
        $data = $request->except('file');
        if ($request->file) {
            $file_name = uploadFile($request->file, 'subject');
            if ($file_name) {
                $data['file'] = $file_name;
            }
        }
        $data['added_by'] = $request->user()->id;
        $this->subject->fill($data);
        $success = $this->subject->save();
        if ($success) {
            $request->session()->flash('success', 'Subject  added successfully.');
        } else {
            $request->session()->flash('error', 'Sorry! There wss a problem while adding subject.');
        }
        return redirect()->route('subject.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->subject = $this->subject->find($id);
        if (!$this->subject) {
            request()->session()->flash('error', 'Subject does  not exists.');
            return redirect()->route('subject.index');
        }
        return view('admin.subject-form')->with('data', $this->subject);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->subject = $this->subject->find($id);
        if (!$this->subject) {
            request()->session()->flash('error', 'School does  not exists.');
            return redirect()->route('subject.index');
        }

        $rules = $this->subject->getRules("update");
        $request->validate($rules);
        $data = $request->except('file');
        if ($request->file) {
            $subject_name = uploadFile($request->file, 'subject');
            if ($subject_name) {
                $data['subject'] = $subject_name;
                deleteFile($this->subject->file, 'subject');
            }
        }

        $this->subject->fill($data);
        $success = $this->subject->save();
        if ($success) {
            $request->session()->flash('success', 'Subject updated successfully.');
        } else {
            $request->session()->flash('error', 'Sorry! There was a problem while updating  subject.');
        }
        return redirect()->route('subject.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {


        $this->subject = $this->subject->find($id);
        if (!$this->subject) {
            request()->session()->flash('error', 'Subject does  not exists.');
            return redirect()->route('subject.index');
        }
        $subject = $this->subject->subject;
        $del = $this->subject->delete();
        if ($del) {
            deleteFile($subject, 'subject');
            request()->session()->flash('success', 'Subject deleted successfully.');

        } else {
            request()->session()->flash('error', 'Sorry! there was a problem while deleting subject.');
        }
        return redirect()->route('subject.index');

    }
}
