<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use phpDocumentor\Reflection\Types\Nullable;

class School extends Model
{
    protected $fillable =['name','address','telephone','email','image','status','added_by'];
   public function created_by(){
        return $this->hasOne('App\User','id','added_by');
  }

   public function  getRules($act = "add"){
       $rules=[
           'name'=>'required|string',
           'address'=>'required|string',

           'telephone'=>'required|string',
           'email'=> 'required|string|email|unique:schools,email',
           'status'=>'required|in:active,inactive',
           'image'=>'required|image|max:5000'

      ];

        if ($act !='add'){
            $rules['image'] ="sometimes|image|max:5000";
            $rules['email']= 'nullable|string|email|unique:schools,email';
        }

     return $rules;
   }
}
