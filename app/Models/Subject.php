<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $fillable =['title','file','description','status','added_by'];
    public function created_by(){
        return $this->hasOne('App\User','id','added_by');
    }

    public function  getRules(){
        $rules=[
            'title'=>'required|string',
            'description'=>'nullable|string',
            'status'=>'required|in:active,inactive',
            'file'=>'nullable|file'

        ];


        return $rules;
    }
}
