<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
     protected $fillable = ['gradename','status','added_by'];

    public function created_by(){
        return $this->hasOne('App\User','id','added_by');
    }

     public function getrules($act = "add"){
         $rules =[

             'gradename'=>'required|string',
             'status'=> 'required|in:active,inactive'
         ];



         return $rules;
     }

}
