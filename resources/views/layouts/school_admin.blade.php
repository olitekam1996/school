
@include('school_admin.section.header')
  @include('school_admin.section.top-nav')

   @include('school_admin.section.sidebar')

    <div class="content-wrapper">
        <!-- START PAGE CONTENT-->
        <div class="page-content fade-in-up">

      @yield('content')

        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
  @include('school_admin.section.footer')

