<!-- START SIDEBAR-->
<nav class="page-sidebar" id="sidebar">
    <div id="sidebar-collapse">
        <div class="admin-block d-flex">
            <div>
                <i class="fa fa-user fa-2x" style="color: #ffffff"></i>
            </div>
            <div class="admin-info">
                <div class="font-strong">{{auth()->user()->name}}</div><small>{{ucfirst(auth()->user()->role)}}</small></div>
        </div>
        <ul class="side-menu metismenu">
            <li>
                <a class="active" href="{{route('home')}}"><i class="sidebar-item-icon fa fa-th-large"></i>
                    <span class="nav-label">Dashboard</span>
                </a>
            </li>
            <li class="heading">FEATURES</li>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-school"></i>
                    <span class="nav-label">School Detail</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="{{route('school.create')}}">Add School </a>
                    </li>
                    <li>
                        <a href="{{route('school.index')}}">List School </a>
                    </li>



                </ul>
            </li>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fas fa-person-booth"></i>
                    <span class="nav-label">
                       Class
                    </span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="{{route('grade.create')}}">Add Class</a>
                    </li>
                    <li>
                        <a href="{{route('grade.index')}}">List Class</a>
                    </li>



                </ul>
            </li>

            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-book-reader"></i>
                    <span class="nav-label">Subject</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="{{route('subject.create')}}">Subject Add</a>
                    </li>

                    <li>
                        <a href="{{route('subject.index')}}">Subject List</a>
                    </li>
--}}

                </ul>
            </li>


        </ul>
    </div>
</nav>
<!-- END SIDEBAR-->
