@extends('layouts/admin')
@section('title','School Profile',' |School.com')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-name"><strong >School Profile</strong></div>
                    <div class="ibox-tools">

                    </div>
                </div>
                <div class="ibox-body"style="line-height: 50px">

                    <div class="container" >
                        @if($school)
                            @foreach($school as $key=>$info)
                        <div class="row">
                            <div class="col-sm-2" style="font-weight: bold">School Name:</div>
                            <div class="col-sm-10">{{ $info->name }}</div>
                        </div>

                        <div class="row">
                            <div class="col-sm-2" style="font-weight: bold">School Address:</div>
                            <div class="col-sm-10">{{ $info->address }}</div>
                        </div>



                        <div class="row">
                            <div class="col-sm-2"style="font-weight: bold">School Telephone:</div>
                            <div class="col-sm-10">{{ $info->telephone }}</div>
                        </div>

                        <div class="row">
                            <div class="col-sm-2"style="font-weight: bold">School Email:</div>
                            <div class="col-sm-10">{{ $info->email }}</div>
                        </div>

                        <div class="row">
                            <div class="col-sm-2"style="font-weight: bold">School Logo:</div>
                            <div class="col-sm-10">
                                <img src="{{ getImage('school', $info->image) }}" alt="">
                            </div>
                        </div>
                            @endforeach
                            @endif
                    </div>

                </div>

            </div>
        </div>
    </div>
@endsection
