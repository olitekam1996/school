@extends('layouts/admin')
@section('title','Subject' .(isset($data) ? '  Update':  'Add'). ' |Subject.com')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-name">Subject {{isset($data) ? 'Update' : 'Add'}} Form</div>
                    <div class="ibox-tools">

                    </div>
                </div>
                <div class="ibox-body">

                    @if(isset($data))
                        {{ Form:: open(['url'=>route('subject.update',$data->id),'class'=>'form','files'=>true])}}
                        @method('patch')
                    @else
                        {{ Form:: open(['url'=>route('subject.store'),'class'=>'form','files'=>true])}}

                    @endif


                    <div class="form-group row">
                        {{Form::label('title','Subject Name:',['class'=>'col-sm-3'])}}
                        <div class="col-sm-9">
                            {{Form::text('title',@$data->title,['class'=>' form-control form-control-sm','id'=>'title','require'=>true,'placeholder'=>'Enter title Name... '])}}
                            @error('title')
                            <span class="alert-danger"></span>
                            @enderror
                        </div>
                    </div>


                        <div class="form-group row">
                            {{Form::label('description','Subject Description:',['class'=>'col-sm-3'])}}
                            <div class="col-sm-9">
                                {{Form::textarea('description',@$data->description,['class'=>' form-control form-control-sm','id'=>'description','require'=>true,])}}
                                @error('description')
                                <span class="alert-danger"></span>
                                @enderror
                            </div>
                        </div>



                        <div class="form-group row">
                        {{Form::label('status','Status:',['class'=>'col-sm-3'])}}
                        <div class="col-sm-9">
                            {{Form::select('status',['active'=>'Published','inactive'=>'Un-Published'],@$data->status,['class'=>'form-control form-control-sm','id'=>'status','required'=>true])}}
                            @error('status')
                            <span class="alert-danger">{{$message}}}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        {{Form::label('file','File:',['class'=>'col-sm-3'])}}
                        <div class="col-sm-4">
                            {{Form::file('file',['id'=>'file','required'=>(isset($data) ? false : true),'accept'=>'file'])}}
                            @error('file')
                            <span class="alert-danger">{{$message}}}</span>
                            @enderror
                        </div>

                    </div>

                    <div class="form-group row">
                        {{Form::label('','',['class'=>'col-sm-3'])}}
                        <div class="col-sm-9">
                            {{Form::button('<i class="fa fa-trash"></i> Reset',['class'=>'btm btn-danger btn-sm','type'=>'reset'])}}
                            {{Form::button('<i class="fa fa-paper-plane"></i> Submit',['class'=>'btm btn-success btn-sm','type'=>'submit'])}}

                        </div>
                    </div>

                    {{ Form:: close() }}
                </div>

            </div>
        </div>
    </div>
@endsection
