@extends('layouts/admin')
@section('title','Class' .(isset($data) ? '  Update':  'Add'). ' |School.com')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-name">Class {{isset($data) ? 'Update' : 'Add'}} Form</div>
                    <div class="ibox-tools">

                    </div>
                </div>
                <div class="ibox-body">

                    @if(isset($data))
                        {{ Form:: open(['url'=>route('grade.update',$data->id),'class'=>'form','files'=>true])}}
                        @method('patch')
                    @else
                        {{ Form:: open(['url'=>route('grade.store'),'class'=>'form','files'=>true])}}

                    @endif


                    <div class="form-group row">
                        {{Form::label('gradename','Grade Name:',['class'=>'col-sm-3'])}}
                        <div class="col-sm-9">
                            {{Form::text('gradename',@$data->gradename,['class'=>' form-control form-control-sm','id'=>'gradename','require'=>true,'placeholder'=>'Enterschool Garde Name... '])}}
                            @error('gradename')
                            <span class="alert-danger"></span>
                            @enderror
                        </div>
                    </div>



                    <div class="form-group row">
                        {{Form::label('status','Status:',['class'=>'col-sm-3'])}}
                        <div class="col-sm-9">
                            {{Form::select('status',['active'=>'Active','inactive'=>'In-Active'],@$data->status,['class'=>'form-control form-control-sm','id'=>'status','required'=>true])}}
                            @error('status')
                            <span class="alert-danger">{{$message}}}</span>
                            @enderror
                        </div>
                    </div>




                    <div class="form-group row">
                        {{Form::label('','',['class'=>'col-sm-3'])}}
                        <div class="col-sm-9">
                            {{Form::button('<i class="fa fa-trash"></i> Reset',['class' => 'btm btn-danger btn-sm','type'=>'reset'])}}
                            {{Form::button('<i class="fa fa-paper-plane"></i> Submit',['class'=>'btm btn-success btn-sm','type'=>'submit'])}}

                        </div>
                    </div>

                    {{ Form:: close() }}
                </div>

            </div>
        </div>
    </div>
@endsection
