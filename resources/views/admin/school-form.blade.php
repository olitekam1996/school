@extends('layouts/admin')
@section('title','School' .(isset($data) ? '  Update':  'Add'). ' |School.com')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-name">School {{isset($data) ? 'Update' : 'Add'}} Form</div>
                    <div class="ibox-tools">

                    </div>
                </div>
                <div class="ibox-body">

                    @if(isset($data))
                        {{ Form:: open(['url'=>route('school.update',$data->id),'class'=>'form','files'=>true])}}
                        @method('patch')
                    @else
                        {{ Form:: open(['url'=>route('school.store'),'class'=>'form','files'=>true])}}

                    @endif


                    <div class="form-group row">
                        {{Form::label('name','Name:',['class'=>'col-sm-3'])}}
                        <div class="col-sm-9">
                            {{Form::text('name',@$data->name,['class'=>' form-control form-control-sm','id'=>'name','require'=>true,'placeholder'=>'Enterschool Schoo Name... '])}}
                            @error('name')
                            <span class="alert-danger"></span>
                            @enderror
                        </div>
                    </div>

                        <div class="form-group row">
                            {{Form::label('address','Address:',['class'=>'col-sm-3'])}}
                            <div class="col-sm-9">
                                {{Form::text('address',@$data->address,['class'=>' form-control form-control-sm','id'=>'address','require'=>true,'placeholder'=>'Enter School Address... '])}}
                                @error('address')
                                <span class="alert-danger">{{$message}}}</span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row">
                            {{Form::label('telephone','Telephone:',['class'=>'col-sm-3'])}}
                            <div class="col-sm-9">
                                {{Form::text('telephone',@$data->telephone,['class'=>' form-control form-control-sm','id'=>'telephone','require'=>true,'placeholder'=>'Enter school Telephone... '])}}
                                @error('telephone')
                                <span class="alert-danger">{{$message}}}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            {{Form::label('email','Email:',['class'=>'col-sm-3'])}}
                            <div class="col-sm-9">
                                {{Form::text('email',@$data->email,['class'=>' form-control form-control-sm',(isset($data)?'disabled':true),'id'=>'email','require'=>false,])}}
                                @error('email')
                                <span class="alert-danger">{{$message}}}</span>
                                @enderror
                            </div>
                        </div>



                    <div class="form-group row">
                        {{Form::label('status','Status:',['class'=>'col-sm-3'])}}
                        <div class="col-sm-9">
                            {{Form::select('status',['active'=>'Published','inactive'=>'Un-Published'],@$data->status,['class'=>'form-control form-control-sm','id'=>'status','required'=>true])}}
                            @error('status')
                            <span class="alert-danger">{{$message}}}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        {{Form::label('image','School logo:',['class'=>'col-sm-3'])}}
                        <div class="col-sm-4">
                            {{Form::file('image',['id'=>'image','required'=>(isset($data) ? false : true),'accept'=>'image/&'])}}
                            @error('image')
                            <span class="alert-danger">{{$message}}}</span>
                            @enderror
                        </div>
                        @if(isset($data))
                            <div class="col-sm-4">
                                <img src="{{asset('uploads/school/Thumb-'.$data->image)}}" alt="{{$data->name}}" class="img img-fluid img-thumbnail">
                            </div>
                        @endif
                    </div>

                    <div class="form-group row">
                        {{Form::label('','',['class'=>'col-sm-3'])}}
                        <div class="col-sm-9">
                            {{Form::button('<i class="fa fa-trash"></i> Reset',['class'=>'btm btn-danger btn-sm','type'=>'reset'])}}
                            {{Form::button('<i class="fa fa-paper-plane"></i> Submit',['class'=>'btm btn-success btn-sm','type'=>'submit'])}}

                        </div>
                    </div>

                    {{ Form:: close() }}
                </div>

            </div>
        </div>
    </div>
@endsection
