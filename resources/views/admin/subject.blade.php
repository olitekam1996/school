@extends('layouts/admin')
@section('title')
    Subject List Page |School.com
@endsection

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title">Subject List</div>
                    <div class="ibox-tools">

                    </div>
                </div>
                <div class="ibox-body">
                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Subject Name</th>
                            <th>Description</th>
                             <th>File</th>
                            <th>Status</th>
                            <th>Added By</th>
                            <th width="91px">Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        @if($data)
                            @foreach($data as $key=>$info)

                                <tr >
                                    <td>{{$info->title}}</td>

                                    <td>{{$info->description}}</td>
                                        <td>
                                           @if($info->file != null)
                                                @if(file_exists(public_path().'/uploads/files/subject/'.$info->file))
                                                    {{ $info->file }}
                                                @else
                                                    No file found.
                                                @endif
                                            @else
                                                No File uploaded
                                            @endif

                                        </td>


                                    <td>
                                        <span class="badge badge-{{($info->status == 'active') ? 'success':'danger'}}">
                                            {{ucfirst($info->status =='active')?'Active':'Inactive' }}
                                        </span>
                                    </td>
                                    <td>{{$info->created_by['name']}}</td>
                                    <td>
                                        <a href="{{route('subject.edit',$info->id)}}" class="btn btn-success flote-left btn-sm btn-rounded" >
                                            <i class="fa fa-edit"></i>

                                        </a>





                                        {{Form::open(['url'=>route('subject.destroy',$info->id),'class'=>'form','onsubmit'=>'return confirm("Are you sure you want to delete?")']) }}
                                        @method('delete ')
                                        <button type="submit" class="btn btn-danger flote-right btn-sm btn-rounded " style="margin-top:5px; " >
                                            <i class="fa fa-trash"></i>
                                        </button>
                                        {{Form::close()}}


                                    </td>
                                </tr>
                            @endforeach
                        @endif


                        </tbody>
                    </table>

                    {{$data->links()}}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')

    <script >
        $(document).ready(function(){
            $('.table').DataTable({
                /* "order": ['id', 'asc']*/
            });
        });
    </script>
    <script >
        $('.detail').on('click',function () {
            document.location.href = `${document.location.href}/${$(this).data('id')}`;
        })
    </script>
@endsection
