@extends('layouts/admin')
@section('title')
    School List Page |School.com
@endsection

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title">School List</div>
                    <div class="ibox-tools">

                    </div>
                </div>
                <div class="ibox-body">
                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>School Name</th>
                            <th>Address</th>

                            <th>Telephone</th>
                            <th>Email</th>

                            <th>Status</th>
                            <th>Added By</th>
                            <th width="91px">Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        @if($data)
                            @foreach($data as $key=>$info)

                                <tr >
                                    <td class="detail" data-id="{{$info->id}}">{{$info->name}}</td>
                                    <td>{{$info->address}}</td>
                                   {{-- <td>{{$info->location}}</td>--}}
                                    <td>{{$info->telephone}}</td>
                                    <td>{{$info->email}}</td>



                                {{--    <td>

                                       @if($info->image != null)
                                            @if(file_exists(public_path().'/uploads/school/Thumb-'.$info->image))
                                                <img src="{{asset('uploads/school/Thumb-'.$info->image)}}" style="max-width:150px" alt="{{$info->title}}" class="img-thumbnail img-fluid">
                                            @elseif(file_exists(public_path().'/uploads/school/'.$info->image))
                                                <img src="{{asset('uploads/school/'.$info->image)}}"style="max-width: 150px" alt="{{$info->title}}" class="img-thumbnail img-fluid">
                                            @else
                                                No image found.
                                            @endif
                                        @else
                                            No Image uploaded
                                        @endif

                                    </td>--}}


                                    <td>
                                        <span class="badge badge-{{($info->status == 'active') ? 'success':'danger'}}">
                                            {{ucfirst($info->status =='active')?'Published':'Un-published' }}
                                        </span>
                                    </td>
                                    <td>{{$info->created_by['name']}}</td>
                                    <td>
                                        <a href="{{route('school.edit',$info->id)}}" class="btn btn-success flote-left btn-sm btn-rounded" >
                                            <i class="fa fa-edit"></i>

                                        </a>





                                        {{Form::open(['url'=>route('school.destroy',$info->id),'class'=>'form','onsubmit'=>'return confirm("Are you sure you want to delete?")']) }}
                                        @method('delete ')
                                        <button type="submit" class="btn btn-danger flote-right btn-sm btn-rounded " style="margin-top:5px; " >
                                            <i class="fa fa-trash"></i>
                                        </button>
                                        {{Form::close()}}


                                    </td>
                                </tr>
                            @endforeach
                        @endif


                        </tbody>
                    </table>

                    {{$data->links()}}
                </div>
            </div>
        </div>
    </div>
    @endsection
@section('scripts')

    <script >
        $(document).ready(function(){
            $('.table').DataTable({
               /* "order": ['id', 'asc']*/
            });
        });
    </script>
    <script >
        $('.detail').on('click',function () {
         document.location.href = `${document.location.href}/${$(this).data('id')}`;
        })
    </script>
@endsection
