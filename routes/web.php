<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');

});


Route::group(['prefix'=>'admin','middleware'=>['auth','admin']],function (){
    Route::get('/','HomeController@admin')->name('admin');

    Route::resource('school','SchoolController');
    Route::resource('grade','GradeController');
    Route::resource('subject','SubjectController');



 });




Route::group(['prefix'=>'school','middleware'=>['auth','school']],function (){
    Route::get('/','HomeController@school')->name('school');


});

Route::group(['prefix'=>'parent','middleware'=>['auth','parent']],function (){
    Route::get('/','HomeController@parent')->name('parent');
 });


Auth::routes(['register'=>false]);

Route::get('/home', 'HomeController@index')->name('home');
